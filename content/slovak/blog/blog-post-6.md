---
title: "Katedrálny organový festival 2022"
date: 2022-08-24T11:07:10+06:00
author: Lukas Kiss
image : "rocnik/rocnik6/plagat.jpeg"
bg_image: "images/organ.jpg"
categories: ["ročník"]
tags: ["2022"]
description: "ročník 2022"
draft: false
type: "post"
---
## Brožúra na stiahnutie
<a href="/Brozura_KOF_2022.pdf" target="_blank" class="btn btn-main animated fadeInUp">Stiahnuť Brožúru </a>   


# Nedeľa 4. september 2022 / 19.00
{{<figure class="post-media post-thumb" src="/rocnik/rocnik6/interpreti/Pier_Damiano_Peretti_Photo1.jpeg" >}}
### **Pier Damiano Peretti** – organ / Rakúsko

# Nedeľa 11. september / 19.00
{{<figure class="post-media post-thumb" src="/rocnik/rocnik6/interpreti/Paolo_Oreni_photo.jpeg" >}}
### **Paolo Oreni** – organ / Taliansko

# Nedeľa 18. september / 19.00
{{<figure class="post-media post-thumb" src="/rocnik/rocnik6/interpreti/Vladimir_Kopec_foto.jpeg" >}}
### **Vladimír Kopec** – organ / Slovensko

{{<figure class="post-media post-thumb" src="/rocnik/rocnik6/interpreti/ZOE_photo.jpeg" >}}
### **ZOE** – NITRIANSKY KOMORNÝ ORCHESTER ZOE / Slovensko

# Streda 21. september / 18.00
{{< align center >}} **Sólisti** {{< /align >}}
{{< skladatel meno="Hilda Gulyás" rok="soprán">}}
{{< skladatel meno="Angelika Zajícová" rok="alt">}}
{{< skladatel meno="Matúš Šimko" rok="tenor">}}
{{< skladatel meno="Tomáš Šelc" rok="bas">}}


# Nedeľa 25. september / 19.00
{{<figure class="post-media post-thumb" src="/rocnik/rocnik6/interpreti/Thierry_Mechler_photo.jpeg" >}}
### **THIERRY MECHLER** – organ / Francúzsko