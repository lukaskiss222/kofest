---
title: "Katedrálny organový festival 2020"
date: 2020-07-24T11:07:10+06:00
author: Lukas Kiss
image : "rocnik/rocnik4/plagat.jpg"
bg_image: "images/organ.jpg"
categories: ["ročník"]
tags: ["2020"]
description: "ročník 2020"
draft: false
type: "post"
---

# Nedeľa 6. septembra 2020
### **Jaroslav Tůma** – organ / Česká republika
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Adam Václav Michna" rok="(1700 - 1779)">}}
{{< skladby >}}
**Loutna česká** - výber {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Jaroslav Tůma " rok="(*1956)">}}
{{< skladby >}}
**Loutna česká** na témy Adama Václava Michnu – výber: {{< br >}}
**Hymnus** {{< br >}}
**Předmluva - Barkarola** {{< br >}}
**Hymnus** {{< br >}}
**Matky Boží slavná Nadaní - Perpetuum mobile** {{< br >}}
**Hymnus** {{< br >}}
**Smutek bláznivých Panen - Fantasia in a** {{< br >}}
{{< /skladby >}} 


{{< skladatel meno="Johann Sebastian Bach " rok="(1685-1750)">}}
{{< skladby >}}
**Choräle von verschiedener Art** – tzv. **Schüblerove chorály:**  {{< br >}}
**Wachet auf, ruft uns die Stimme**, BWV 645 {{< br >}}
**Wo soll ich fliehen hin**, BWV 646 {{< br >}}
**Wer nur den lieben Gott lässt walten**, BWV 647 {{< br >}}
**Meine Seele erhebt den Herren**, BWV 648 {{< br >}}
**Ach bleib bei uns, Herr Jesu Christ**, BWV 649 {{< br >}}
**Kommst du nun, Jesu, vom Himmel herunter**, BWV 650 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach: " rok="">}}
{{< skladby >}}
**Toccata a fúga F dur**, BWV 540 {{< br >}}
{{< /skladby >}} 

{{<figure class="post-media post-thumb" src="/rocnik/rocnik4/interpreti/tuma.JPG" >}}



# Nedeľa 13. septembra 2020
### **Axel Flierl**, organ / Nemecko
{{< align center >}} **PROGRAM** {{< /align >}}


{{< skladatel meno="Johann Sebastian Bach " rok="(1685-1750)">}}
{{< skladby >}}
**Meine Seele erhebt den Herrn**, BWV 733 {{< br >}}
*Fuga sopra il Magnificat pro organo pleno* {{< br >}}
Trio super **Allein Gott in der Höh’ sei Ehr** à 2 claviers et pédale, BWV 664 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="François Couperin " rok="(1688-1733)">}}
{{< skladby >}}
**Messe pour les convents** {{< br >}}
Plein Jeu. Premier couplet du Kyrie {{< br >}}
Fugue sur la Trompette. 2e couplet du Kyrie {{< br >}}
Récit de Chromorne {{< br >}}
Dialogue. 5e couplet du Kyrie {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Georg Böhm " rok="(1661-1733)">}}
{{< skladby >}}
**Vater unser im Himmelreich** á 2 Clav. et Pedal {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Georg Friedrich Händel " rok="(1685-1759)">}}
{{< skladby >}}
**Pieces for a Musical Clock** {{< br >}}
**A Voluntary on a Flight of Angels**, HWV 600 {{< br >}}
**Sonata**, HWV 598 {{< br >}}
**Air**, HWV 601 {{< br >}}
**Gigue**,  HWV 589 {{< br >}}
**Menuet**, HWV 603 {{< br >}}
**Air** (Gavotte), HWV 604 {{< br >}}
**Gigue**, HWV 599 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="">}}
{{< skladby >}}
**Toccata und Fuge d moll**, BWV 538 {{< br >}}
{{< /skladby >}}

{{<figure class="post-media post-thumb" src="/rocnik/rocnik4/interpreti/axel.jpg" >}}

# Nedeľa, 20. september 2020
### **Stanislav Šurin** - organ / Slovensko
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Johann Caspar Kerll " rok="(1627 – 1693)">}}
{{< skladby >}}
**Battaglia** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Juan Cabanilles" rok="(1644 – 1712)">}}
{{< skladby >}}
**Corrente italiana** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Jozef Podprocký " rok="(*1944)">}}
{{< skladby >}}
**Malá suita** {{< br >}}
*(podľa zápiskov v levočskom* {{< br >}}
*„Pestrom zborníku“ zo 17. storočia)* {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="František Xaver Brixi " rok="(1732-1771)">}}
{{< skladby >}}
**Prelúdium C dur** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Anonymus" rok="">}}
{{< skladby >}}
**Fúga a mol** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Baldassare Galuppi " rok="(1706-1785)">}}
{{< skladby >}}
**Sonata in d** {{< br >}}
*Andante – Allegro – Largo – Allegro e spiritoso* {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Georg Friedrich Händel" rok="(1685-1759)">}}
{{< skladby >}}
**Voluntary III.** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Benedetto Marcello " rok="(1686-1736)">}}
{{< skladby >}}
**Adagio** {{< br >}}
*(transkripcia pre organ: J. S. Bach)*
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="(1685-1750)">}}
{{< skladby >}}
**Prelúdium Es dur**, BWV 552, 1 {{< br >}}
**Kommst du nun, Jesu**, BWV 650 {{< br >}}
**Fúga Es dur**, BWV 552, 2 {{< br >}}
{{< /skladby >}}

{{<figure class="post-media post-thumb" src="/rocnik/rocnik4/interpreti/surin.jpg" >}}

# Nedeľa 27. septembra 2020
### **Johannes Radschiner** – organ / Rakúsko
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Clement Loret " rok="(1833-1909)">}}
{{< skladby >}}
**Scherzo Fanfare** Op. 45 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Josef Gabriel Rheinberger " rok="(1839-1901)">}}
{{< skladby >}}
**Orgelsonate** Nr. 8 e-mol, Op. 132{{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Felix Mendelssohn Bartholdy " rok="(1809-1847)">}}
{{< skladby >}}
**Präludium und Fuge d moll**, Op. 37, Nr. 3 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Whitney Eugene Thayer " rok="(1838-1889)">}}
{{< skladby >}}
**Variations on Auld Lang Syne**, Op. 30 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="(1685-1750)">}}
{{< skladby >}}
**Praeludium und Fuge C dur**, BWV 547 {{< br >}}
{{< /skladby >}}

{{<figure class="post-media post-thumb" src="/rocnik/rocnik4/interpreti/radschiner.jpg" >}}

# Nedeľa 4. októbra 2020
### **Geraud Guillemot** – organ / Francúzsko
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Dietrich Buxtehude " rok="(1637-1706)">}}
{{< skladby >}}
**Praeludium, Fuge und Ciacona C dur**, BuxWV 137 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Anonym" rok="">}}
{{< skladby >}}
**Cancion para la corneto con el eco** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Pablo Bruna " rok="(1611-1679)">}}
{{< skladby >}}
**Tiento de tiples** {{< br >}}
**Tiento de baixo** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Francisco Correa de Arauxo " rok="(1584-1654)">}}
{{< skladby >}}
**Tiento de medio registro de tiple 6 tono** {{< br >}}
**Tiento de medio registro de bajo** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Andres De Solas " rok="(1634-1696)">}}
{{< skladby >}}
**Tiento de medio registro de mano derechia** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Dietrich Buxtehude" rok="">}}
{{< skladby >}}
**Nun lob mein Seel den Herren (4 partite)** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="François Couperin " rok="(1668-1733)">}}
{{< skladby >}}
**Messe à l'usage des convents** (výber ) {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Dietrich Buxtehude:" rok="">}}
{{< skladby >}}
**Präludium und Fuga g mol**, BuxWV 149 {{< br >}}
{{< /skladby >}} 

{{<figure class="post-media post-thumb" src="/rocnik/rocnik4/interpreti/guillemot.jpg" >}}