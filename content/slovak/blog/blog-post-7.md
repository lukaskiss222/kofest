---
title: "Katedrálny organový festival 2023"
date: 2023-08-24T11:07:10+06:00
author: Lukas Kiss
image : "rocnik/rocnik7/plagat2023.png"
bg_image: "images/organ.jpg"
categories: ["ročník"]
tags: ["2023"]
description: "ročník 2023"
draft: false
type: "post"
---

## Brožúra na stiahnutie
<a href="/Brozura_KOF_2023.pdf" target="_blank" class="btn btn-main animated fadeInUp">Stiahnuť Brožúru </a> 


# Nedeľa 3. september 2023 / 19.00
{{<figure class="post-media post-thumb" src="/rocnik/rocnik7/interpreti/Almada_mini.jpeg" >}}
### **Christian Alejandro Almada** – organ / Argentína

# Nedeľa 10. september 2023 / 19.00
{{<figure class="post-media post-thumb" src="/rocnik/rocnik7/interpreti/Puhovichova_mini.JPG" >}}
### **Marieta Puhovichová** – organ / Slovensko

# Nedeľa 17. september 2023 / 19.00
{{<figure class="post-media post-thumb" src="/rocnik/rocnik7/interpreti/Bonetto_mini.jpeg" >}}
### **Roberto Bonetto** – organ / Taliansko

# Nedeľa 24. september 2023 / 19.00
{{<figure class="post-media post-thumb" src="/rocnik/rocnik7/interpreti/Sampedro.jpg" >}}
### **Jesús Sampedro Márquez** – organ /  Španielsko

# Nedeľa 1.  októbra  2023 / 19.00
{{<figure class="post-media post-thumb" src="/rocnik/rocnik7/interpreti/Pronesti_mini.JPG" >}}
### **Salvatore Pronesti** – organ /  Taliansko