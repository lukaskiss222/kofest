---
title: "Katedrálny organový festival 2018"
date: 2018-09-24T11:07:10+06:00
author: Lukas Kiss
image : "rocnik/rocnik2/plagat.jpg"
bg_image: "images/organ.jpg"
categories: ["ročník"]
tags: ["2018"]
description: "ročník 2018"
draft: false
type: "post"
---

# Nedeľa 02. september / 19.30
### **Géraud Guillemot**/ Francúzsko
## **Organové cesty po Európe**
{{< align center >}} **PROGRAM** {{< /align >}}


{{< skladatel meno="Cabanilles anonimo " rok="(17. stor.)">}}
{{< skladby >}}
**Tiento de falsas 4 tono**
{{< /skladby >}} 

{{< skladatel meno="Antonio de Cabezon " rok="(1510–1566)">}}
{{< skladby >}}
**Tiento del primer tono**
{{< /skladby >}} 

{{< skladatel meno="Correa de Arauxo  " rok="(1584–1654)">}}
{{< skladby >}}
**Tiento de medio registro de baxon** {{< br >}}
**Tiento de medio registro de triple 6 tono**
{{< /skladby >}} 

{{< skladatel meno="Domenico Zipoli " rok="(1688–1726)">}}
{{< skladby >}}
**All´elevazione**
{{< /skladby >}} 

{{< skladatel meno="Girolamo Frescobaldi " rok="(1583–1643)">}}
{{< skladby >}}
**Toccata per l´elevazione**
{{< /skladby >}} 

{{< skladatel meno="Marcel Godard " rok="(1920–2007)">}}
{{< skladby >}}
**La nuit qu´il fut livré** {{< br >}}
**Je crois que mon Sauveur**
{{< /skladby >}} 

{{< skladatel meno="Scheidt Samuel " rok="(1587–1654)">}}
{{< skladby >}}
**Variationen über eine Gagliarda Dowland**
{{< /skladby >}} 

{{< skladatel meno="Johann Pachelbel " rok="(1653–1706)">}}
{{< skladby >}}
**Komm Gott Schöpfer, Heiliger Geist**
{{< /skladby >}} 

{{< skladatel meno="Gottfried Homilius " rok="(1714–1785)">}}
{{< skladby >}}
**Trio**
{{< /skladby >}} 

{{< skladatel meno="Georg Böhm" rok="(1661–1733)">}}
{{< skladby >}}
**(1661–1733):Vater unser im Himmelreich**
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="(1685–1750)">}}
{{< skladby >}}
Sinfonia cantate **Actus tragicus**
{{< /skladby >}} 

{{< skladatel meno="Jan Pieterszoon Sweelinck " rok="(1562–1621)">}}
{{< skladby >}}
**Toccata** {{< br >}}
**Fantasia auf die Manier ein Echo**
{{< /skladby >}} 

{{< skladatel meno="Nicolaus Bruhns " rok="(1665–1697)">}}
{{< skladby >}}
**Präludium und Fuga g moll**
{{< /skladby >}} 

{{<figure class="post-media post-thumb" src="/rocnik/rocnik2/interpreti/guillemot.jpg" >}}

# Nedeľa 09. september / 19.30
### **Manuel TOMADIN** / Taliansko
## **G – E – C  dokonalá božská triáda**
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Johann Sebastian Bach" rok="(1685 – 1750)">}}

*Tónina G*
{{< skladby >}}
**Preludio e Fuga in G Major**,  BWV 541	{{< br >}}						
**Herr Jesu Christ, dich zu uns** a 2 Clav. e Ped., BWV 709 	{{< br >}}			
**Trio Herr Jesu Christ, dich zu uns** a 2 Clav. e Ped., BWV 655				
{{< /skladby >}}

*Tónina E*
{{< skladby >}}
**Preludio e fuga in e minor**, BWV 548   {{< br >}}							
**Jesus Christus Unser Heiland**, BWV 666				{{< br >}}			
**Vater unser im Himmelreich** a 2 Clav. et Ped. BWV 682		
{{< /skladby >}} 

*Tónina E*
{{< skladby >}}
**Kleine harmonisches Labyrint**, BWV 591	{{< br >}}							
**Gelobet seist du, Jesu Christ** a 2 Clav. e Ped., BWV 604				{{< br >}}		
**Preludio e Fuga in C Major**, BWV 545	
{{< /skladby >}}

{{<figure class="post-media post-thumb" src="/rocnik/rocnik2/interpreti/tomadin.JPG" >}}

# Nedeľa 16. september / 19.30
### **Sietze de VRIES** / Holandsko
## **Nizozemské inšpirácie**
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Jan Pieterszoon Sweelinck " rok="(1562–1621)">}}
{{< skladby >}}
**Toccata XX in C** {{< br >}}
**Vater unser im Himmelreich** (3 versus)
{{< /skladby >}} 


{{< skladatel meno="Sietze de Vries " rok="(*1973)">}}
{{< skladby >}}
**Improvisation**
{{< /skladby >}} 

{{< skladatel meno="Dietrich Buxtehude " rok="(1637–1707)">}}
{{< skladby >}}
**Vater unser im Himmelreich** (3 versus) BuxWV 207 {{< br >}}
**Toccata d-moll** BuxWV 155
{{< /skladby >}} 


{{< skladatel meno="Sietze de Vries:" rok="">}}
{{< skladby >}}
**Improvisation**
{{< /skladby >}} 

{{<figure class="post-media post-thumb" src="/rocnik/rocnik2/interpreti/sietze.jpg" >}}

# Nedeľa 23. september / 19.30
### **Gabriele GIACOMELLI** / Taliansko
## **Z Florencie do celého sveta**
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Anonym " rok="">}}
{{< skladby >}}
**Saltarello II** (z rukopisu v London British Library) {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Athanasius Kircher " rok="(1602–1680)">}}
{{< skladby >}}
**Vera Tarantella Tono Hypodorio - Alia clausula**  {{< br >}}
(neapolská tradícia) 
{{< /skladby >}} 

{{< skladatel meno="Georg Muffat " rok="(1653–1704)">}}
{{< skladby >}}
**Passacaglia** g mol {{< br >}}
{{< /skladby >}} 

{{< skladatel meno=" Domenico Zipoli " rok="(1688–1726)">}}
{{< skladby >}}
**Sonata II g mol**  (Preludio - Corrente - Sarabanda - Giga){{< br >}}
(z rukopisu archívu Musical de Chiquitos (Bolívia, prvá polovica 18. stor.) {{< br >}}
**Folias** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="(1685–1750)">}}
{{< skladby >}}
**Sinfonia z Kantáty Ich steh' mit einem Fuss in Grabe,**  {{< br >}} (BWV 156 - Adagio z Koncertu pre clavicembalo, BWV 1056) {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Antonio Vivaldi " rok="(1678–1741)">}}
{{< skladby >}}
**Concerto h mol pre sláčiky, LV133** {{< br >}}
(transkripcia pre organ: Johann Gottfried Walther){{< br >}}
Allegro – Adagio - Allegro {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Anonym " rok="(Florencia, zač. 19. stor.)">}}
{{< skladby >}}
**Elevazione** B dur (z rukopisu Archivio Musicale Katedrály {{< br >}}
Santa Maria del Fiore, Florencia) {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Athanasius Kircher " rok="(1602–1680)">}}
{{< skladby >}}
**Antidotum Tarantulae – Tarentella**  {{< br >}}
(tradícia z regiónuPuglia){{< br >}}
{{< /skladby >}} 

{{<figure class="post-media post-thumb" src="/rocnik/rocnik2/interpreti/giocomelli.jpg" >}}



# Nedeľa 30. september / 19.30
### **Drahomíra MATZNEROVÁ** / Česko
## **Toccaty pred Bachom a po Bachovi**
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Wolfgang Amadeus Mozart " rok="(1756–1791)">}}
{{< skladby >}}
**Fantasie** f moll, KV 608 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Jakob Froberger " rok="(1616–1667)">}}
{{< skladby >}}
**Toccata**  d moll{{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Pietro Domenico Paradies " rok="(1707–1791)">}}
{{< skladby >}}
**Toccata** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="(1685–1750)">}}
{{< skladby >}}
**O, Mensch, bewein dein  Sünde gross**, BWV 622 {{< br >}}
**Toccata a fuga**  d moll, BWV 565{{< br >}}
**Wenn wir in höchsten Nöten sein**, BWV 641 {{< br >}}
**Toccata – dórska**, BWV 538a {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Theodor Dubois " rok="(1837–1924)">}}
{{< skladby >}}
**Toccata** G dur {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johannnes Mathias Michel " rok="(*1962)">}}
{{< skladby >}}
Choral: **Grosser Gott, wir loben dich** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Christopher Pardini " rok="(1974)">}}
{{< skladby >}}
**Toccata on Gospel „Amazing Grace”** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno=" Astor Piazzola " rok="(1921–1992)">}}
{{< skladby >}}
**Ave Maria** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Ernest Hasley " rok="(1876–1939)">}}
{{< skladby >}}
**Toccata**  c moll{{< br >}}
{{< /skladby >}} 

{{<figure class="post-media post-thumb" src="/rocnik/rocnik2/interpreti/matznerov.jpg" >}}