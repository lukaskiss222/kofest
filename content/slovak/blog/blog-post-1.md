---
title: "Katedrálny organový festival 2017"
date: 2017-09-24T11:07:10+06:00
author: Lukas Kiss
image : "rocnik/rocnik1/plagat.JPG"
bg_image: "images/organ.jpg"
categories: ["ročník"]
tags: ["2017"]
description: "ročník 2017"
draft: false
type: "post"
---

# Nedeľa 27. august / 19.30
### **Helmut  HAUSKELLER**, panova flauta / Nemecko
### **Stanislav ŠURIN**, organ / Slovensko
{{< align center >}} **PROGRAM** {{< /align >}}


{{< skladatel meno="Giulio Caccini" rok="(1546 –1618)">}}
{{< skladby >}} 
Ave Maria
{{< /skladby >}}

{{< skladatel meno="Michael Praetorius" rok="(1571 – 1621)">}}
{{< skladby >}} 
Ballet de Coqs	 {{< br >}} 
Spagnoletta
{{< /skladby >}}

{{< skladatel meno="Georg Friedrich Kaufmann" rok="(1679 – 1735)">}}
{{< skladby >}} 
Choral „Nun lob, mein Seel, den Herren” 
{{< /skladby >}}

{{< skladatel meno="Johann Sebastian Bach" rok="(1685-1750)">}}
{{< skladby >}} 
Prelúdium a fúga G dur, BWV 541
{{< /skladby >}}


### EURÓPSKA BAROKOVÁ SUITA	

{{< skladatel meno="Girolamo Fantini" rok="(1600 – 1675)">}}
{{< skladby >}} 
Corrente
{{< /skladby >}}


{{< skladatel meno="Michel Corrette" rok="(1709 – 1795)">}}
{{< skladby >}} 
Tambourin {{< br >}} 
Rondeau
{{< /skladby >}}

{{< skladatel meno="Turlough O´Carolan" rok="(1670 – 1738)">}}
{{< skladby >}} 
Princess Royal
{{< /skladby >}}

{{< skladatel meno="Malte Rühmann" rok="(1960 – 2008)">}}
{{< skladby >}} 
Siciliano
{{< /skladby >}}


{{< skladatel meno="Michel Corrette" rok="(1709 – 1795)">}}
{{< skladby >}} 
Fanfare	
{{< /skladby >}}

{{< skladatel meno="Stanislav Šurin" rok="(*1971)">}}
{{< skladby >}} 
Fanfares
{{< /skladby >}}

{{< skladatel meno="Alan Hovhaness" rok="(1911 – 2000)">}}
{{< skladby >}} 
Prayer of Saint Gregory 
{{< /skladby >}}

{{< skladatel meno="Petr Eben" rok="(1929 – 2007)">}}
{{< skladby >}} 
Klamný příslib zlatého věku (z „Labyrint světa a ráj srdce)
{{< /skladby >}}

{{< skladatel meno="Gaston Litaize" rok="(1909 – 1991)">}}
{{< skladby >}} 
Prélude liturgique
{{< /skladby >}}

{{< skladatel meno="Petr Eben" rok="(1929 – 2007)">}}
{{< skladby >}} 
Nevědomost učených (z „Labyrint světa a ráj srdce)
{{< /skladby >}}

{{< skladatel meno="Peter Heeren" rok="(*1970)">}}
{{< skladby >}} 
Choral „Was Gott tut, das ist wohlgetan” 
{{< /skladby >}}

{{< skladatel meno="Vera Stanislav	" rok="(*1955)">}}
{{< skladby >}} 
Am Abend (le Soir)
{{< /skladby >}}

### ĽUDOVÉ MELÓDIE Z CELÉHO SVETA
- Señora Doña Maria (Čile)								
- Doina din Ardeal (Rumunsko)							
- Gort na Saileán – Down by the Sally Gardens (Írsko)				
- Flieg herbei (Ukraina)								
- Sanie cu Zurgalai (Rumunsko)							
				
{{<figure class="post-media post-thumb" src="/rocnik/rocnik1/interpreti/hurin.jpg" >}}


# Nedeľa 03. september / 19.30
### **Piotr ROJEK** / Poľsko
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Johann Sebastian Bach" rok="(1685-1750)">}}
{{< skladby >}} 
Chorálová predohra: **Christum wir sollen loben schon**, BWV 611  {{< br >}} 
Partita: **O Gott du frommer Gott**, BWV 767 {{< br >}} 
**Toccata a fúga d mol**, BWV 565
{{< /skladby >}}

{{< skladatel meno="Marian Sawa" rok="(1937-2004)">}}
{{< skladby >}} 
**Toccata Festiva** 
{{< /skladby >}}

{{< skladatel meno="Piotr Rojek   " rok="(*1975)">}}
{{< skladby >}} 
**Improvizácia**  
{{< /skladby >}}

{{<figure class="post-media post-thumb" src="/rocnik/rocnik1/interpreti/rojek.jpg" >}}


# Nedeľa 10. september / 19.30 
### **Klaus Kuchling** / Rakúsko
{{< align center >}} **PROGRAM** {{< /align >}}


{{< skladatel meno="Johann Sebastian Bach" rok="(1685-1750)">}}
{{< skladby >}} 
**Prelúdium a fúga C dur,** BWV 531 {{< br >}} 
**Triová sonáta C dur**, BWV 529, časti: Allegro -- Largo -- Allegro {{< br >}} 
Partite diverse sopra: **Ach, was soll ich Sünder machen** {{< br >}} 
**Fúga c mol**, BWV 574
{{< /skladby >}}


{{< skladatel meno="Wolfgang Amadeus Mozart" rok="(1756-1791)">}}
{{< skladby >}}   **Andante** für aine kleine Orgelwalze F dur, KV 616
{{< /skladby >}} 

{{< skladatel meno="Zsolt Gárdony" rok="(*1946)">}}
{{< skladby >}}  **Mozart Changes** {{< /skladby >}} 


{{< skladatel meno="Alexandre Pierre François Boëly" rok="(1785-1858)">}}
{{< skladby >}}  **Fantázia a fúga B dur**, Op. 18, č. 6 {{< /skladby >}} 

{{<figure class="post-media post-thumb" src="/rocnik/rocnik1/interpreti/klaus.jpg" >}}

# Nedeľa 17. september  / 19.30
### **Daniele PARUSSINI** / Taliansko
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Johann Sebastian Bach" rok="(1685-1750)">}}
{{< skladby >}} 
**Prelúdium a fúga d mol**, BWV 539  {{< br >}} 
Chorálová predohra **Schmücke dich, o liebe Seele** BWV 654 	 {{< br >}} 
**Fantázia g mol**, BWV 542, 1 
{{< /skladby >}}

{{< skladatel meno="Georg Böhm" rok="(1661-1733)">}}
{{< skladby >}}  
Partita **Ach wie nichtig, ach wie flüchtig**
{{< /skladby >}} 

{{< skladatel meno="Sigfrid Karg-Elert" rok="(1877-1933)">}}
{{< skladby >}} 
**Ein Siegesgesang Israels - Alla Händel** {{< br >}}
Z cyklu „33 Portrétov pre Harmónium” Op. 101
{{< /skladby >}}

{{< skladatel meno="Gabriel Pierné" rok="(1863-1937)">}}
{{< skladby >}}  **Prelúdium** z Trois piéces, Op. 29, č. 1 
{{< /skladby >}}

{{< skladatel meno="Jean Langlais" rok="(1907-1991)">}}
{{< skladby >}}  
**Canzona** zo „Suite Folkloric"        
{{< /skladby >}} 

{{< skladatel meno="Mons Leidvin Tackle" rok="(*1942)">}}
{{< skladby >}}  
**The earth of peace**    
{{< /skladby >}} 

{{< skladatel meno="Denis Bédard" rok="(*1950)">}}
{{< skladby >}}  
**Variácie na „Christus vincit”**      
{{< /skladby >}}

{{<figure class="post-media post-thumb" src="/rocnik/rocnik1/interpreti/parussini.jpg" >}}

# Nedeľa 24. september  / 19.30
### **Tibor ĎURIAK**, trúbka / Slovensko
### **Vladimír KOPEC**, organ / Slovensko
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Tomaso Albinoni" rok="(1671-1751)">}}
{{< skladby >}} 
**Koncert d mol** pre trúbku a organ, Op. 9 č. 2, časti:  {{< br >}}
*Allegro e non presto  -  Adagio -  Allegro*
{{< /skladby >}}

{{< skladatel meno="Mikuláš Schneider-Trnavský" rok="(1881-1958)">}}
{{< skladby >}}  
**Maestoso** f mol   
{{< /skladby >}}

{{< skladatel meno="Carl Philip Telemann" rok="(1681-1767)">}}
{{< skladby >}}  
**Sonata F dur** pre trúbku a organ, časti: {{< br >}}
*Vivace – Largo- Allegro*   
{{< /skladby >}}

{{< skladatel meno="Domenico Bartolucci" rok="(1917-2013)">}}
{{< skladby >}}  
**Prelúdium a fúga c mol** 
{{< /skladby >}}

{{< skladatel meno="Antonio Vivaldi/J. S. Bach" rok="">}}
{{< skladby >}}  
**Koncert D dur** pre trúbku a organ, BWV 972, časti:
*Allegro - Larghetto  - Allegro* 
{{< /skladby >}}

{{<figure class="post-media post-thumb" src="/rocnik/rocnik1/interpreti/kopec.jpg" >}}