---
title: "Katedrálny organový festival 2021"
date: 2021-07-24T11:07:10+06:00
author: Lukas Kiss
image : "rocnik/rocnik5/plagat.png"
bg_image: "images/organ.jpg"
categories: ["ročník"]
tags: ["2021"]
description: "ročník 2021"
draft: false
type: "post"
---


# Nedeľa 29. august  / 19:00

{{<figure class="post-media post-thumb" src="/rocnik/rocnik5/interpreti/Markuszewski.jpeg" >}}
### **Michat MARKUSZEWSKI** – organ / Poľsko, Varšava

# Nedeľa 05. september / 19:00

{{<figure class="post-media post-thumb" src="/rocnik/rocnik5/interpreti/nicolo.jpg" >}}
### **Nicolò Antonio** – organ / Taliansko, Benátky

# Nedeľa 12. september / 19:00

{{<figure class="post-media post-thumb" src="/rocnik/rocnik5/interpreti/lecian.jpg" >}}
### **Jiri Lecian** – organ / Taliansko, Rím

# Nedeľa 19. september / 19:00

{{<figure class="post-media post-thumb" src="/rocnik/rocnik5/interpreti/marzec.jpg" >}}
### **Renata MARZEC** – violoncello / Poľsko, Bydgoszcz



