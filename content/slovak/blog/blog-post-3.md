---
title: "Katedrálny organový festival 2019"
date: 2019-09-24T11:07:10+06:00
author: Lukas Kiss
image : "rocnik/rocnik3/plagat.jpg"
bg_image: "images/organ.jpg"
categories: ["ročník"]
tags: ["2019"]
description: "ročník 2019"
draft: false
type: "post"
---

# Nedeľa 01. september / 19.30
## V Bazilike sv. Emeráma Nitra
### **Géraud Guillemot**/ Francúzsko
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Pablo Bruna " rok="(1611-1679)">}}
{{< skladby >}}
**Tiento sobre la letania de la Virgen** {{< br >}}
**Tiento de tiples** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Anonym " rok="(Španielsko 16. stor.)">}}
{{< skladby >}}
**Cancion para la corneto con el eco** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Francisco Peraza " rok="(1564-1598)">}}
{{< skladby >}}
**Medio registro Alto de Primer tono** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Andres De Solas " rok="(1634-1696)">}}
{{< skladby >}}
**Tiento de medio registro de derechia** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Anonym " rok="(Španielsko 16. stor.)">}}
{{< skladby >}}
**Batalla Famosa** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Samuel Scheidt " rok="(1587-1654)">}}
{{< skladby >}}
**Christ lag in Todes Banden** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Dietrich Buxtehude " rok="(1637-1706)">}}
{{< skladby >}}
**Wie Schön leuchtet der Morgenstern** {{< br >}}
**Nun lob mein Seel den Herren (4 partite)** {{< br >}}
**Prelúdium, fúga a Ciacona C dur** {{< br >}}
{{< /skladby >}} 


{{< skladatel meno="Gottfried Homilius " rok="(1714-1785)">}}
{{< skladby >}}
Choral: **Straf mich nicht in deinem Zorn** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Nikolaus Bruhns " rok="(1665-1697)">}}
{{< skladby >}}
**Prelúdium a fúga g mol** {{< br >}}
{{< /skladby >}}

{{<figure class="post-media post-thumb" src="/rocnik/rocnik3/interpreti/guillemot.jpg" >}}



# Nedeľa 08. september / 19.30
## V Bazilike sv. Emeráma Nitra
### **Josef KŠICA** / Česká republika
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="František Xaver Brixi " rok="(1732–1771)">}}
{{< skladby >}}
**Prelúdium C dur** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Jan Křtitel Vaňhal " rok="(1739–1813)">}}
{{< skladby >}}
**Fúga c mol** {{< br >}}
**Fúga F dur** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Matthias Weckmann " rok="(1621–1674)">}}
{{< skladby >}}
**Fantázia** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Dietrich Buxtehude " rok="(1637–1707)">}}
{{< skladby >}}
**Fúga in C** {{< br >}}
**Prelúdium a fúga D dur**
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="(1685–1750)">}}
{{< skladby >}}
**Fantázia s imitáciou** h mol, BWV 563 {{< br >}}
**Ich ruf´ zu dir, Herr Jesu Christ**, BWV 639 {{< br >}}
**Fúga  sopra Magnificat**, BWV 733 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Josef Kšica " rok="(*1952)">}}
{{< skladby >}}
**Organová improvizácia na „Salve Regina“** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Klement Slavický " rok="(1910–1999)">}}
{{< skladby >}}
**Invokace** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="John Stanley " rok="(1713–1786)">}}
{{< skladby >}}
**Voluntary X** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="John Robinson " rok="(1682–1762)">}}
{{< skladby >}}
**Voluntary in A minor** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Petr Eben " rok="(1929–2007)">}}
{{< skladby >}}
**Fantasia Corale II** {{< br >}}
{{< /skladby >}}

{{<figure class="post-media post-thumb" src="/rocnik/rocnik3/interpreti/ksica.JPG" >}}



# Nedeľa 15. september / 19.30
## V Bazilike sv. Emeráma Nitra
### **Vladimír KOPEC** – organ / Slovensko
### **Ján GAŠPÁREK** – soprán & tenor saxofón / Slovensko
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Louis Marchand " rok="(1869-1732)">}}
{{< skladby >}}
**Dialogue** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="(1685-1750)">}}
{{< skladby >}}
**Suita č. 3**, BWV 1009 pre violončelo, (transkripcia pre saxofón) {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="François Couperin " rok="(1668 -1733)">}}
{{< skladby >}}
**Offertoire** {{< br >}}
(Messe pour les Convents de Religieux et Religieuses 1690) {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Allessandro Marcello " rok="(1673-1747)">}}
{{< skladby >}}
**Koncert d mol**, Op. 1 pre hoboj a orchester {{< br >}}
(transkripcia pre soprán saxofón a organ) {{< br >}}
*Moderato – Adagio – Allegro* {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="(1685-1750)">}}
{{< skladby >}}
**Schmücke dich, o liebe Seele**, BWV 654 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Gabriel Fauré " rok="(1845-1924)">}}
{{< skladby >}}
**Pavana** Op. 50  {{< br >}}
{{< /skladby >}} 

### **Vladimír KOPEC** – organ / Slovensko
{{<figure class="post-media post-thumb" src="/rocnik/rocnik3/interpreti/kopec.jpg" >}}
### **Ján GAŠPÁREK** – soprán & tenor saxofón / Slovensko
{{<figure class="post-media post-thumb" src="/rocnik/rocnik3/interpreti/gasparek.jpg" >}}

# Piatok 20. september / 19.00
## V Bazilike minor HRONSKÝ BEŇADIK
### **Daniele Ferretti** / Taliansko
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Georg Friedrich Händel " rok="(1685-1759)">}}
{{< skladby >}}
**Koncert č. 13** „Il cuculo e l’usignolo”, HWV 295 {{< br >}}
(transkripcia: K. Schnorr) {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="(1685-1750)">}}
{{< skladby >}}
**Prelúdium a fúga c mol**, BWV 549 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Felix Mendelsshon Bartoldy " rok="(1809-1847)">}}
{{< skladby >}}
**Sonata III**, op. 65, časti:{{< br >}}
*Con moto maestoso – Andante tranquillo* {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Josef Gabriel Rheinbergher " rok="(1839-1901)">}}
{{< skladby >}}
**Cantilene** zo Sonáty č. 11, Op. 148 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Marco Enrico Bossi " rok="(1861-1925)">}}
{{< skladby >}}
**Ave Maria**, Op. 104, č. 2 {{< br >}}
**Stunde der Freude**, Op. 132 No. 5 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Pietro Alessandro Yon " rok="(1886-1943)">}}
{{< skladby >}}
**Ave Maria** *skomponovaná v Piemonte* {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Antonio Vivaldi / Anonym" rok="(1678-1741)">}}
{{< skladby >}}
**Koncert č. 5**, RV 519 G dur {{< br >}}
Zo zbierky pre sláčiky a orchester (transkripcia pre organ), časti: {{< br >}}
*Allegro – Largo – Allegro* {{< br >}}
{{< /skladby >}}

{{<figure class="post-media post-thumb" src="/rocnik/rocnik3/interpreti/ferretti.jpg" >}}

# Nedeľa 22. september / 19.30
## V Bazilike sv. Emeráma Nitra
### **Daniele Ferretti** / Taliansko
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Georg Friedrich Händel " rok="(1685-1759)">}}
{{< skladby >}}
**Koncert č. 13** „Il cuculo e l’usignolo”, HWV 295 {{< br >}} 
(transkripcia: K. Schnorr) {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="(1685-1750)">}}
{{< skladby >}}
**Allein Gott in der Höh' sei Ehr’**, BWV 662 {{< br >}}
**Prelúdium a fúga C dur**, BWV 531 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Felix Mendelssohn Bartoldy " rok="(1809-1847)">}}
{{< skladby >}}
**Sonata III, op. 65**, časti: {{< br >}}
*Con moto maestoso – Andante tranquillo* {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Vincenzo Petrali " rok="(1830-1889)">}}
{{< skladby >}}
**Sonata D dur** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Marco Enrico Bossi " rok="(1861-1925)">}}
{{< skladby >}}
**Stunde der Freude**, Op. 132 No. 5 {{< br >}}
**Chant du soir**, Op. 92 No. 1 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Edgard J. Bellerby " rok="(1858-1940)">}}
{{< skladby >}}
**Toccata D dur** {{< br >}}
{{< /skladby >}} 

{{<figure class="post-media post-thumb" src="/rocnik/rocnik3/interpreti/ferretti.jpg" >}}

# Nedeľa 29. september / 19.30
## V Bazilike sv. Emeráma Nitra
### **Christian Tarabbia** / Taliansko
{{< align center >}} **PROGRAM** {{< /align >}}

{{< skladatel meno="Heinrich Scheidemann " rok="">}}
{{< skladby >}}
**Benedicam Domino** (z moteta H. Praetoria) {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Anonym " rok="(Taliansko, 17. stor.)">}}
{{< skladby >}}
**Aria di fiorenza**  (z rukopisu: Doria Pamphili){{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Dietrich Buxtehude " rok="(1637-1707)">}}
{{< skladby >}}
**Ciaccona e mol**, BuxWV 160 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Antonio Vivaldi " rok="(1678-1741)">}}
{{< skladby >}}
**Sonata** No. 12, Op. 1 “**La Follia**” (transkripcia: C. Tarabbia) {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Sebastian Bach " rok="(1685-1750)">}}
{{< skladby >}}
Aria: **Schafe können sicher weiden** z Kantáty BWV 208{{< br >}}
**Prelúdium a fúga D dur**, BWV 532 {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Gottfried Walther " rok="(1684-1748)">}}
{{< skladby >}}
Partite sopra: **Jesu meine Freude** {{< br >}}
{{< /skladby >}} 

{{< skladatel meno="Johann Ludwig Krebs " rok="(1713-1780)">}}
{{< skladby >}}
**Prelúdium a fúga C dur** {{< br >}}
{{< /skladby >}}

{{<figure class="post-media post-thumb" src="/rocnik/rocnik3/interpreti/tarabbia.jpg" >}}